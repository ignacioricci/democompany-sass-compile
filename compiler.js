'use strict';

// Require dependencies
var fs = require('fs');
var path = require('path');
var sass = require('node-sass');
var colors = require('colors');

// CSS Directories (Use outStyle "nested" to not minify the output)
var sassType = 'scss';
var inFile = path.join(__dirname, '..', '..', 'styles', sassType, 'index.' + sassType);
var outDir = path.join(__dirname, '..', '..', 'styles', 'css');
var outFile = outDir + '/index.css';
var outStyle = 'compressed';
var env = process.argv.reverse()[0];
if(env != 'production'){
  env = 'development';
  outStyle = 'nested';
}

// Render Sass
exports.renderSass = function(){

  var sassData = fs.readFile(inFile, function(err, data) {
    if (err){
      console.log('Error:'.bgRed + ' ' + inFile.red + ' does not exist! Please create it and run the command again.'.red);
    }
    else {
      // Render CSS
      sass.render({
        file: inFile,
        outputStyle: outStyle,
        data: '$environment:' + env + ';' + data
      }, function(err, css) {
        if (err){
          console.error(err);
        }
        else {
          fs.writeFileSync(outFile, css.css, 'utf8');
          console.log('Compiled:'.bgGreen + ' ' + inFile.gray + ' into ' + outFile.green);
        }
      });
    }

  });
  
}