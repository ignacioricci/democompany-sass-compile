'use strict';

// Require dependencies
var path = require('path');
var compile = require('./compiler.js');
var chokidar = require('chokidar');
var colors = require('colors');

// Render for the first time before watching
compile.renderSass();

// Watch directory
var sassType = 'scss';
var watchDir = path.join(__dirname, '..', '..', 'styles', sassType);

// Watcher
function watcher(type, fn){
  chokidar
    .watch(watchDir, {persistent: true})
    .on('ready', function(path){
      console.log('Watching:'.bgYellow + ' ' + watchDir + ' directory...'.yellow);
    })
    .on('change', function(path, stats) {
      if (fn != ''){
        fn();
      }
      console.log(path, stats);
    });
}

// Run watch
watcher('sass', compile.renderSass);

// Print errors
process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err);
});